package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.Unit;

import java.util.List;

/**
 * @description 商品类别
 */
public interface GoodsTypeDao {

    List<GoodsType> getAllGoodsTypeByParentId(Integer pId);

    Integer updateGoodsTypeState(GoodsType parentGoodsType);

    //查询商品单位
    List<Unit> getUnit();

    //删除分类
    void deleteGoodsType(Integer goodsTypeId);
    //新增分类
    void saveGoodsType(GoodsType goodsType);



}
