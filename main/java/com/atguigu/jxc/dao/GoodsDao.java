package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.*;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品信息
 */
public interface GoodsDao {


    String getMaxCode();

    /**
     * 分页查询商品库存信息
     *
     * @param page
     * @param rows
     * @param codeOrName
     * @param goodsTypeId
     * @return
     */
    List<Goods> getMerChandise(Integer page, Integer rows, String codeOrName, Integer goodsTypeId);

    /**
     * 分页查询供应商
     *
     * @param page
     * @param rows
     * @param supplierName
     * @return
     */
    List<Supplier> getSupplier(Integer page, Integer rows, String supplierName);

    /**
     * 根据用户名查找用户
     * String 商品信息实体
     *
     * @return
     */

    Supplier getSaveName(String supplierName);

    /**
     * 新增供应商
     *
     * @param supplier
     */
    void addGoods(Supplier supplier);

    /**
     * 更新供应商
     *
     * @param supplier
     */
    void updateGoods(Supplier supplier);

    /**
     * 删除供应商
     *
     * @param ids
     */
    void delete(String ids);

    /**
     * 客户列表分页
     *
     * @param offSet
     * @param rows
     * @param customerName
     * @return
     */
    List<Customer> getCustomers(int offSet, Integer rows, String customerName);

    /**
     * 添加客户
     *
     * @param customerName
     * @return
     */
    Supplier getCustomerName(String customerName);

    void addCustomer(Customer customer);

    void updateCustomer(Customer customer);

    /**
     * 删除客户
     *
     * @param ids
     */
    void customerDelete(String ids);

    // 根据用户id查询用户
    Object getSupplierId(String ids);

    //根据ID 查询供应商
    int getSuppId(Integer ids);

    //根据ID 查询商品
    Supplier getGoodsName(String goodsName);

    //添加商品
    void addGoodss(Goods goods);

    //修改商品
    void updateGoodss(Goods goods);

    //删除商品
    void getGoodsDelete(Integer goodsId);

    //查询id
    int GoodsDeleteid(Integer goodsId);

    //根据编码查询
    int getGoodsCode(String nameOrCode);

    //无库存查询
    List<Goods> getNoInventoryQuantity(@Param("offSet") Integer offSet, @Param("rows") Integer rows, @Param("nameOrCode") String nameOrCode);

    //有库存查询
    List<Goods> getHasInventoryQuantity(@Param("offSet") Integer offSet, @Param("rows") Integer rows, @Param("nameOrCode") String nameOrCode);

    //修改数量
    void updateSave(@Param("goodsId") Integer goodsId, @Param("purchasingPrice") double purchasingPrice, @Param("inventoryQuantity") Integer inventoryQuantity);

    //删除库存
    int deleteStock(Goods goods);

    //查询库存
    Goods getStockId(Integer goodsId);

    //保存报损单


    //保存
    int damageListGoodsList(DamageListGoods damageListGood);

    int damageList(DamageList damageList);

    //新增报溢
    void overflowList(OverflowList overflowList);

    int overflowListGoodsList(OverflowListGoods list);

    //查询所有 当前库存量 小于 库存下限的商品信息
    List<Goods> findAll();

    //报损单查询
    List<DamageList> getDamageListGoods(String sTime, String eTime);

    //查询报损单商品信息
    List<DamageListGoods> getDamageListGoodss(Integer damageListId);

    //报溢单查询
    List<OverflowList> getOverflowList(String sTime, String eTime);

    //报溢单商品信息
    List<OverflowListGoods> getOverflowListGoods(Integer overflowListId);
}
