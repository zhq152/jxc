package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.*;

import java.util.Map;

public interface GoodsService {

    ServiceVO getCode();

    // 分页查询商品库存信息
    Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId);

    // 分页查询商品信息
    Map<String, Object> getSupplier(Integer page, Integer rows, String supplierName);

    // 添加或修改商品信息
    ServiceVO save(Supplier supplier);

    // 删除供应商
    ServiceVO delete(String ids);

    //客户列表分页
    Map<String, Object> getCustomers(Integer page, Integer rows, String customerName);

    // 添加客户
    ServiceVO InsetCustomer(Customer customer);

    //删除客户
    ServiceVO customerDelete(String ids);

    //商品添加或修改
    ServiceVO getGoods(Goods goods);

    //商品删除
    ServiceVO getGoodsDelete(Integer goodsId);

    //无库存商品列表展示
    Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    //有库存商品列表展示
    Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode);

    //添加库存、修改数量或成本价
    ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice);

    //删除库存
    ServiceVO deleteStock(Integer goodsId);

    //保存报损单
    ServiceVO damageListGoods(DamageList damageList, String damageListGoodsStr);

    //新增报溢单
    ServiceVO overflowListGoods(OverflowList overflowList, String overflowListGoodsStr);

    //查询所有 当前库存量 小于 库存下限的商品信息
    Map<String, Object> getListAlarm();

    //报损单查询
    Map<String, Object> getDamageListGoods(String sTime, String eTime);

    //查询报损单商品信息
    Map<String, Object> getDamageListGoodss(Integer damageListId);

    //报溢单查询
    Map<String, Object> getOverflowList(String sTime, String eTime);

    //报溢单商品信息
    Map<String, Object> getOverflowListGoods(Integer overflowListId);

}
