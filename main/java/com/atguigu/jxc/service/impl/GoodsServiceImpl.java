package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.*;

import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @description
 */
@Service
@SuppressWarnings("all")
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;
    @Autowired
    private LogService logService;


    @Override
    public ServiceVO getCode() {
        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();
        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;
        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();
        for (int i = 4; i > intCode.toString().length(); i--) {
            unitCode = "0" + unitCode;
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }


    /**
     * 分页查询商品库存信息
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param codeOrName  商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @Override
    public Map<String, Object> listInventory(Integer page, Integer rows, String codeOrName, Integer goodsTypeId) {
        HashMap<String, Object> map = new HashMap<>();
        page = page == 0 ? 0 : page;
        int offSet = (page - 1) * rows;
        List<Goods> goods = goodsDao.getMerChandise(offSet, rows, codeOrName, goodsTypeId);
        map.put("rows", goods);
        return map;
    }


    /**
     * 分页查询商品信息
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param goodsName   商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */


    /**
     * 分页查询供应商
     *
     * @param page         当前页
     * @param rows         每页显示条数
     *                     goodsName   商品名称
     *                     goodsTypeId 商品类别ID
     * @param supplierName
     * @return
     */
    @Override
    public Map<String, Object> getSupplier(Integer page, Integer rows, String supplierName) {
        HashMap<String, Object> map = new HashMap<>();
        page = page == 0 ? 0 : page;
        int offSet = (page - 1) * rows;
        List<Supplier> supplier = goodsDao.getSupplier(offSet, rows, supplierName);
        map.put("rows", supplier);
        return map;
    }

    /**
     * 添加或修改商品信息
     *
     * @param goods 商品信息实体
     * @return
     */
    @Override
    public ServiceVO save(Supplier supplier) {
        if (supplier.getSupplierId() == null) {
            Supplier saveId = goodsDao.getSaveName(supplier.getSupplierName());
            if (saveId != null) {
                return new ServiceVO(ErrorCode.ACCOUNT_EXIST_CODE, ErrorCode.ACCOUNT_EXIST_MESS);
            }
            goodsDao.addGoods(supplier);
            logService.save(new Log(Log.INSERT_ACTION, "添加用户:" + supplier.getSupplierName()));
        } else {
            goodsDao.updateGoods(supplier);
            logService.save(new Log(Log.UPDATE_ACTION, "修改用户:" + supplier.getSupplierName()));
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    /**
     * 删除供应商
     *
     * @param supplierId
     * @return
     */
    @Override
    public ServiceVO delete(String ids) {
        String[] split = ids.split(",");
        List<String> asList = Arrays.asList(split);
        for (String supplierId : asList) {
            goodsDao.customerDelete(supplierId);
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    /**
     * 客户列表分页
     *
     * @param page
     * @param rows
     * @param customerName
     * @return
     */
    @Override
    public Map<String, Object> getCustomers(Integer page, Integer rows, String customerName) {
        HashMap<String, Object> map = new HashMap<>();
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Customer> customers = goodsDao.getCustomers(offSet, rows, customerName);
        map.put("rows", customers);
        return map;
    }

    /**
     * 添加客户
     *
     * @param customer
     * @return
     */
    @Override
    public ServiceVO InsetCustomer(Customer customer) {

        if (customer.getCustomerId() == null) {
            Supplier saveId = goodsDao.getCustomerName(customer.getCustomerName());
            if (saveId != null) {
                return new ServiceVO(ErrorCode.ACCOUNT_EXIST_CODE, ErrorCode.ACCOUNT_EXIST_MESS);
            }
            goodsDao.addCustomer(customer);
            logService.save(new Log(Log.INSERT_ACTION, "添加用户:" + customer.getCustomerName()));
        } else {
            goodsDao.updateCustomer(customer);
            logService.save(new Log(Log.UPDATE_ACTION, "修改用户:" + customer.getCustomerName()));
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);

    }

    /**
     * 删除客户
     *
     * @param ids
     * @return
     */
    @Override
    public ServiceVO customerDelete(String ids) {
        String[] split = ids.split(",");
        List<String> asList = Arrays.asList(split);
        for (String supplierId : asList) {
            goodsDao.delete(supplierId);
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    /**
     * 商品添加或修改
     *
     * @param goods
     * @return
     */
    @Override
    public ServiceVO getGoods(Goods goods) {
        if (goods.getGoodsId() == null) {
            Supplier saveId = goodsDao.getGoodsName(goods.getGoodsName());
            if (saveId != null) {
                return new ServiceVO(ErrorCode.ACCOUNT_EXIST_CODE, ErrorCode.ACCOUNT_EXIST_MESS);
            }
            goodsDao.addGoodss(goods);
            logService.save(new Log(Log.INSERT_ACTION, "添加商品:" + goods.getGoodsName()));
        } else {

            goodsDao.updateGoodss(goods);
            logService.save(new Log(Log.UPDATE_ACTION, "修改商品:" + goods.getGoodsName()));
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);

    }

    /**
     * 商品删除
     *
     * @param goodsId
     * @return
     */
    @Override
    public ServiceVO getGoodsDelete(Integer goodsId) {
        int id = goodsDao.GoodsDeleteid(goodsId);
        if (id == 0) {
            return new ServiceVO<>(ErrorCode.ROLE_DEL_ERROR_CODE, ErrorCode.ROLE_DEL_ERROR_MESS);
        }
        goodsDao.getGoodsDelete(goodsId);
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    /**
     * 无库存商品列表展示
     *
     * @param page
     * @param rows
     * @param nameOrCode
     * @return
     */
    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {

        HashMap<String, Object> map = new HashMap<>();
        int total = goodsDao.getGoodsCode(nameOrCode);
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Goods> goods = goodsDao.getNoInventoryQuantity(offSet, rows, nameOrCode);
        map.put("total", total);
        map.put("rows", goods);
        return map;
    }

    /**
     * 有库存商品列表展示
     *
     * @param page
     * @param rows
     * @param nameOrCode
     * @return
     */
    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        HashMap<String, Object> map = new HashMap<>();
        int total = goodsDao.getGoodsCode(nameOrCode);
        page = page == 0 ? 1 : page;
        int offSet = (page - 1) * rows;
        List<Goods> goods = goodsDao.getHasInventoryQuantity(offSet, rows, nameOrCode);
        map.put("total", total);
        map.put("rows", goods);
        return map;
    }

    /**
     * 添加库存、修改数量或成本价
     *
     * @param goodsId
     * @param inventoryQuantity
     * @param purchasingPrice
     * @return
     */
    @Override
    public ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {

        goodsDao.updateSave(goodsId, purchasingPrice, inventoryQuantity);

        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);

    }

    /**
     * 删除库存
     *
     * @param goodsId
     * @return
     */
    @Override
    public ServiceVO deleteStock(Integer goodsId) {
        Goods goods = goodsDao.getStockId(goodsId);
        if (goods.getState() == 0) {
            goods.setInventoryQuantity(0);
            int index = goodsDao.deleteStock(goods);
            if (index > 0) {
                return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
            }
        }
        return null;
    }

    /**
     * 保存报损单
     *
     * @param damageList
     * @param damageListGoodsStr
     * @return
     */
    @Override
    public ServiceVO damageListGoods(DamageList damageList, String damageListGoodsStr) {
        goodsDao.damageList(damageList);
        Integer damageListId = damageList.getDamageListId();
        if (damageListId != null) {
            List<DamageListGoods> damageListGoodsList = JSONObject.parseArray(damageListGoodsStr, DamageListGoods.class);
            for (DamageListGoods damageListGood : damageListGoodsList) {
                damageListGood.setDamageListId(damageListId);
                int rows = goodsDao.damageListGoodsList(damageListGood);
                if (rows > 0) {
                    return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
                }
            }
        }
        return null;
    }

    /**
     * 新增报溢单
     *
     * @param overflowList
     * @param overflowList1
     * @return
     */
    @Override
    public ServiceVO overflowListGoods(OverflowList overflowList, String overflowListGoodsStr) {
        goodsDao.overflowList(overflowList);
        Integer overflowListId = overflowList.getOverflowListId();
        if (overflowListId != null) {
            List<OverflowListGoods> overflowLists = JSONObject.parseArray(overflowListGoodsStr, OverflowListGoods.class);
            for (OverflowListGoods list : overflowLists) {
                list.setOverflowListId(overflowListId);
                goodsDao.overflowListGoodsList(list);
            }
        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    /**
     * 查询所有 当前库存量 小于 库存下限的商品信息
     *
     * @return
     */
    @Override
    public Map<String, Object> getListAlarm() {

        HashMap<String, Object> map = new HashMap<>();
        List<Goods> goods = goodsDao.findAll();
        map.put("rows", goods);
        return map;

    }

    /**
     * 报损单查询
     *
     * @param sTime
     * @param eTime
     * @return
     */
    @Override
    public Map<String, Object> getDamageListGoods(String sTime, String eTime) {
        HashMap<String, Object> map = new HashMap<>();
        List<DamageList> GamageList = goodsDao.getDamageListGoods(sTime, eTime);
        logService.save(new Log(Log.SELECT_ACTION, "报损单查询"));
        map.put("rows", GamageList);
        return map;
    }

    /**
     * 查询报损单商品信息
     *
     * @param damageListId
     * @return
     */
    @Override
    public Map<String, Object> getDamageListGoodss(Integer damageListId) {
        HashMap<String, Object> map = new HashMap<>();
        List<DamageListGoods> listGoodsList = goodsDao.getDamageListGoodss(damageListId);
        map.put("rows", listGoodsList);
        return map;
    }

    /**
     * 报溢单查询
     * @param sTime
     * @param eTime
     * @return
     */
    @Override
    public Map<String, Object> getOverflowList(String sTime, String eTime) {
        HashMap<String, Object> map = new HashMap<>();
        List<OverflowList> overflowList = goodsDao.getOverflowList(sTime, eTime);
        logService.save(new Log(Log.SELECT_ACTION, "报溢单查询"));
        map.put("rows", overflowList);
        return map;
    }

    /**
     * 报溢单商品信息
     * @param overflowListId
     * @return
     */
    @Override
    public Map<String, Object> getOverflowListGoods(Integer overflowListId) {
        HashMap<String, Object> map = new HashMap<>();
        List<OverflowListGoods> overflowListGoods = goodsDao.getOverflowListGoods(overflowListId);
        map.put("rows", overflowListGoods);
        return map;
    }


}
