package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;

import java.util.ArrayList;
import java.util.Map;

/**
 * @description
 */
public interface GoodsTypeService {

    //查询所有商品类别
    ArrayList<Object> loadGoodsType();

    //查询所有商品单位
    Map<String, Object> getUnit();

    //新增分类
    ServiceVO saveGoodsType(String goodsTypeName, Integer pId);

    //删除分类
    ServiceVO deleteGoodsType(Integer goodsTypeId);

}
