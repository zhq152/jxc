package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.GoodsService;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Map;

/**
 * @description 商品信息Controller
 */
@RestController
@RequestMapping("")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    /**
     * 分页查询商品库存信息
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param codeOrName  商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @PostMapping("/goods/listInventory")
    public Map<String, Object> listInventory(Integer goodsTypeId, Integer page, Integer rows, String codeOrName) {
        return goodsService.listInventory(page, rows, codeOrName, goodsTypeId);
    }

    /**
     * 分页查询商品信息
     *
     * @param page 当前页
     * @param rows 每页显示条数
     *             goodsName   商品名称
     *             goodsTypeId 商品类别ID
     * @return
     */
    @PostMapping("/supplier/list")
    public Map<String, Object> getSupplier(Integer page, Integer rows, String supplierName) {
        return goodsService.getSupplier(page, rows, supplierName);
    }

    /**
     * 生成商品编码
     *
     * @return
     */
    @RequestMapping("goods/getCode")
    @RequiresPermissions(value = "商品管理")
    public ServiceVO getCode() {
        return goodsService.getCode();
    }

    /**
     * 添加或修改商品信息
     *
     * @param supplier 商品信息实体
     * @return
     */
    @RequestMapping("supplier/save")
    @ResponseBody
    public ServiceVO save(Supplier supplier) {
        return goodsService.save(supplier);
    }

    /**
     * 删除商品信息
     *
     * @param ids 商品ID
     * @return
     */
    @RequestMapping("supplier/delete")
    @ResponseBody
    public ServiceVO delete(String ids) {
        return goodsService.delete(ids);
    }

    /**
     * 分页查询无库存商品信息
     *
     * @param page         当前页
     * @param rows         每页显示条数
     * @param customerName 商品名称或商品编码
     * @return
     */
    @RequestMapping("customer/list")
    @ResponseBody
    public Map<String, Object> getCustomers(Integer page, Integer rows, String customerName) {
        return goodsService.getCustomers(page, rows, customerName);
    }

    /**
     * 分页查询有库存商品信息
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */


    /**
     * 添加商品期初库存
     * <p>
     * goodsId           商品ID
     * inventoryQuantity 库存
     * purchasingPrice   成本价
     *
     * @return
     */
    @RequestMapping("customer/save")
    @ResponseBody
    public ServiceVO InsetCustomer(Customer customer) {
        return goodsService.InsetCustomer(customer);
    }

    /**
     * 删除商品库存
     *
     * @param ids 商品ID
     * @return
     */
    @RequestMapping("customer/delete")
    @ResponseBody
    public ServiceVO customerDelete(String ids) {
        return goodsService.customerDelete(ids);
    }


    /**
     * 查询所有商品
     *
     * @return
     */
    @PostMapping("goods/list")
    public Map<String, Object> getGoods(Integer page, Integer rows, String goodsName, Integer goodsTypeId) {
        return goodsService.listInventory(page, rows, goodsName, goodsTypeId);
    }

    /**
     * 商品添加或修改
     *
     * @param goods
     * @return
     */
    @PostMapping("goods/save")
    public ServiceVO getGoods(Goods goods) {
        return goodsService.getGoods(goods);
    }

    /**
     * 商品删除
     *
     * @param goodsId
     * @return
     */
    @PostMapping("goods/delete")
    public ServiceVO getGoodsDelete(Integer goodsId) {
        return goodsService.getGoodsDelete(goodsId);
    }


    /**
     * 无库存商品列表展示
     *
     * @param page
     * @param rows
     * @param nameOrCode
     * @return
     */
    @PostMapping("goods/getNoInventoryQuantity")
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        return goodsService.getNoInventoryQuantity(page, rows, nameOrCode);
    }

    /**
     * 有库存商品列表展示
     *
     * @param page
     * @param rows
     * @param nameOrCode
     * @return
     */
    @PostMapping("goods/getHasInventoryQuantity")
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows, String nameOrCode) {
        return goodsService.getHasInventoryQuantity(page, rows, nameOrCode);
    }

    /**
     * 添加库存、修改数量或成本价
     *
     * @param goodsId
     * @param inventoryQuantity
     * @param purchasingPrice
     * @return
     */
    @PostMapping("goods/saveStock")
    public ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        return goodsService.saveStock(goodsId, inventoryQuantity, purchasingPrice);
    }

    /**
     * 删除库存
     *
     * @param goodsId
     * @return
     */
    @PostMapping("goods/deleteStock")
    public ServiceVO deleteStock(Integer goodsId) {
        return goodsService.deleteStock(goodsId);
    }

    /**
     * 保存报损单
     *
     * @param damageList
     * @param damageListGoodsStr
     * @return
     */
    @PostMapping("damageListGoods/save")
    public ServiceVO damageListGoods(DamageList damageList, String damageListGoodsStr) {
        return goodsService.damageListGoods(damageList, damageListGoodsStr);
    }

    /**
     * 新增报溢单
     *
     * @param overflowList
     * @param overflowListGoodsStr
     * @return
     */
    @PostMapping("overflowListGoods/save")
    public ServiceVO overflowListGoods(OverflowList overflowList, String overflowListGoodsStr) {
        return goodsService.overflowListGoods(overflowList, overflowListGoodsStr);
    }

    /**
     * 查询所有 当前库存量 小于 库存下限的商品信息
     *
     * @return
     */
    @PostMapping("goods/listAlarm")
    public Map<String, Object> getListAlarm() {
        return goodsService.getListAlarm();
    }

    /**
     * 报损单查询
     *
     * @param sTime
     * @param eTime
     * @return
     */
    @PostMapping("damageListGoods/list")
    public Map<String, Object> getDamageListGoods(String sTime, String eTime) {
        return goodsService.getDamageListGoods(sTime, eTime);
    }

    /**
     * 查询报损单商品信息
     *
     * @param damageListId
     * @return
     */
    @PostMapping("damageListGoods/goodsList")
    public Map<String, Object> getDamageListGoods(Integer damageListId) {
        return goodsService.getDamageListGoodss(damageListId);
    }

    /**
     * 报溢单查询
     *
     * @param sTime
     * @param eTime
     * @return
     */
    @PostMapping("overflowListGoods/list")
    public Map<String, Object> getOverflowList(String sTime, String eTime) {
        return goodsService.getOverflowList(sTime, eTime);
    }

    /**
     * 报溢单商品信息
     * @param overflowListId
     * @return
     */
    @PostMapping("overflowListGoods/goodsList")
    public Map<String, Object> getOverflowListGoods(Integer overflowListId){
        return goodsService.getOverflowListGoods(overflowListId);
    }
}
